package com.wordpress.rasanti.lightdream.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.wordpress.rasanti.lightdream.base.LightDreamGame;
import com.wordpress.rasanti.lightdream.controllers.DesktopController;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		config.width = 1280;
//		config.height = 720;
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new LightDreamGame(new DesktopController()), config);
	}
}
