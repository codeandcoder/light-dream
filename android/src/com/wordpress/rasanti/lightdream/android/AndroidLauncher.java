package com.wordpress.rasanti.lightdream.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.wordpress.rasanti.lightdream.base.LightDreamGame;
import com.wordpress.rasanti.lightdream.controllers.AndroidController;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new LightDreamGame(new AndroidController()), config);
	}
}
