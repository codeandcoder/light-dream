package com.wordpress.rasanti.lightdream.utils;

import com.badlogic.gdx.math.Vector2;

public class Utils {

    public static double distance(Vector2 a, Vector2 b) {
        return Math.sqrt(Math.pow((a.x - b.x),2) + Math.pow(a.y - b.y, 2));
    }
    
    public static double angle(Vector2 a, Vector2 b) {
        // http://www.vitutor.com/geometry/vec/angle_vectors.html
        return Math.toDegrees(Math.acos((a.x * b.x + a.y * b.y) / (Math.sqrt(Math.pow(a.x, 2) + Math.pow(a.y, 2)) * Math.sqrt(Math.pow(b.x, 2) + Math.pow(b.y, 2)))));
    }
    
}
