package com.wordpress.rasanti.lightdream.utils;

import java.util.Comparator;

import com.wordpress.rasanti.lightdream.gameobjects.GameObject;

public class DrawingComparator implements Comparator<GameObject> {

    @Override
    public int compare(GameObject arg0, GameObject arg1) {
        int res = arg0.getDrawPriority() - arg1.getDrawPriority();
        return res == 0 ? 1 : res;
    }

}
