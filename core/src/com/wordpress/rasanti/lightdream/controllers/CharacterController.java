package com.wordpress.rasanti.lightdream.controllers;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;

public abstract class CharacterController {

    protected PlayableCharacter character;
    
    public CharacterController() {}
    
    public CharacterController(PlayableCharacter character) {
        this.character = character;
    }
    
    public void setCharacter(PlayableCharacter character) {
    	this.character = character;
    }
    
    public PlayableCharacter getCharacter() {
        return character;
    }
    
    public abstract void manageInput(float deltaTime);
    public abstract boolean someInputReceived();
    
    public void switchMode() {
        if (character instanceof Charlie) {
            character = WorldController.getInstance().light;
            WorldController.getInstance().cameraHelper.setTarget(character);
            WorldController.getInstance().light.setTarget(null);
            WorldController.getInstance().light.setCollision(false);
        } else if (character instanceof Light) {
            character = WorldController.getInstance().player;
            WorldController.getInstance().cameraHelper.setTarget(character);
            WorldController.getInstance().light.setTarget(character);
        }
    }
    
    public void freezeLight() {
        if (character instanceof Charlie) {
            WorldController.getInstance().light.setTarget(character);
            WorldController.getInstance().light.setCollision(false);
        } else if (character instanceof Light) {
            character = WorldController.getInstance().player;
            WorldController.getInstance().cameraHelper.setTarget(character);
            WorldController.getInstance().light.setTarget(null);
            WorldController.getInstance().light.setCollision(true);
            WorldController.getInstance().light.getPhysics().setLinearVelocity(new Vector2(0,0));
            WorldController.getInstance().light.animCounter = 0;
        }
    }
    
}
