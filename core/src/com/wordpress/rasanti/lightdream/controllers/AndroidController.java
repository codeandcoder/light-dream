package com.wordpress.rasanti.lightdream.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.Light;

public class AndroidController extends CharacterController {

	@Override
	public void manageInput(float deltaTime) {
		if (character != null) {
		    for ( int i = 0; i < 2; i++) {
		        if (Gdx.input.isTouched(i)) {
		            Vector3 point = new Vector3(Gdx.input.getX(i), Gdx.input.getY(i), 0);
		            WorldController.getInstance().camera.unproject(point);
		            if (character instanceof Charlie) {
    	                if (character.getAspect().getBoundingRectangle().contains(point.x, point.y)) {
    	                    Charlie charlie = (Charlie) character;
    	                    charlie.jump();
    	                } else if (WorldController.getInstance().light.getAspect().getBoundingRectangle().contains(point.x, point.y)) {
    	                    switchMode();
                        } else {
                        	if (point.x > character.getAspect().getX()) {
                                character.move(1.1f, character.getPhysics().getLinearVelocity().y, deltaTime);
                            } else if (point.x < character.getAspect().getX()) {
                                character.move(-1.1f, character.getPhysics().getLinearVelocity().y, deltaTime);
                            }
                        }
		            } else if (character instanceof Light) {
		                if (character.getAspect().getBoundingRectangle().contains(point.x, point.y)) {
                            freezeLight();
                        } else  if (WorldController.getInstance().player.getAspect().getBoundingRectangle().contains(point.x, point.y)) {
                            switchMode();
                        } else {
                        	float movingX = character.getPhysics().getLinearVelocity().x;
                        	float movingY = character.getPhysics().getLinearVelocity().y;
                        	if (point.x > character.getAspect().getX() + character.getAspect().getBoundingRectangle().getWidth() / 2) {
                        		movingX = 1.1f;
                            } else if (point.x < character.getAspect().getX() - character.getAspect().getBoundingRectangle().getWidth() / 2) {
                            	movingX = -1.1f;
                            }
                        	if (point.y > character.getAspect().getY() + character.getAspect().getBoundingRectangle().getHeight() / 2) {
                        		movingY = 1.1f;
                        	} else if (point.y < character.getAspect().getY() - character.getAspect().getBoundingRectangle().getHeight() / 2) {
                        		movingY = -1.1f;
                        	}
                        	character.move(movingX, movingY, deltaTime);
                        }
		            }
	            }
		    }
		}
	}

	@Override
	public boolean someInputReceived() {
		return Gdx.input.isTouched();
	}

}
