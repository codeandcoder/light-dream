package com.wordpress.rasanti.lightdream.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;

public class DesktopController extends CharacterController {
    
	public DesktopController() {
		super();
	}
	
    public DesktopController(PlayableCharacter character) {
        super(character);
    }
    
    @Override
    public void manageInput(float deltaTime) {
    	if (character != null) {
    	    if (character instanceof Charlie) {
    	        if (Gdx.input.isKeyJustPressed(Keys.W)) {
                    Charlie charlie = (Charlie) character;
                    charlie.jump();
                }
    	        if (Gdx.input.isKeyJustPressed(Keys.S)) {
    	            // Crouch
                }
    	    } else if (character instanceof Light) {
    	        if (Gdx.input.isKeyPressed(Keys.W)) {
                   character.move(character.getPhysics().getLinearVelocity().x, 1.1f, deltaTime);
                }
    	        if (Gdx.input.isKeyPressed(Keys.S)) {
                   character.move(character.getPhysics().getLinearVelocity().x, -1.1f, deltaTime);
                }
    	    }
    	    
    	    // Common controls
	        if (Gdx.input.isKeyPressed(Keys.A)) {
	            character.move(-1.1f, character.getPhysics().getLinearVelocity().y, deltaTime);
	        } else if (Gdx.input.isKeyPressed(Keys.D)) {
	            character.move(1.1f, character.getPhysics().getLinearVelocity().y, deltaTime);
	        }
	        
	        if (Gdx.input.isKeyJustPressed(Keys.F)) {
	            switchMode();
	        }
	        
	        if (Gdx.input.isKeyJustPressed(Keys.E)) {
                freezeLight();
            }
    	}
    }

	@Override
	public boolean someInputReceived() {
		return Gdx.input.isKeyPressed(Keys.ANY_KEY) || Gdx.input.isButtonPressed(Buttons.LEFT) || Gdx.input.isButtonPressed(Buttons.MIDDLE) || Gdx.input.isButtonPressed(Buttons.RIGHT);
	}
    
}
