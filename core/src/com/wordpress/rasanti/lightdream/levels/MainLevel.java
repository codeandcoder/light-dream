package com.wordpress.rasanti.lightdream.levels;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.events.Event;
import com.wordpress.rasanti.lightdream.gameobjects.events.SomeInputEvent;
import com.wordpress.rasanti.lightdream.gui.GUI;

public class MainLevel extends Level {
    
    @Override
    public List<GameObject> init() {
        List<GameObject> objects = super.init();

        Light light = new Light("light", new Vector2(0,-0.1f));
        objects.add(light);
        
        return objects;
    }

    @Override
    public String getFile() {
        return null;
    }

    @Override
    public List<Event> getEvents() {
    	List<Event> events = new ArrayList<Event>();
    	events.add(new SomeInputEvent("input", new Vector2(-1,-1), new Vector2(2,2), 0.1f));
        return events;
    }

	@Override
	public void onObjectsInitialized(List<GameObject> objects, WorldController world) {
		GUI.getInstance().setScreen("main");
	}

}
