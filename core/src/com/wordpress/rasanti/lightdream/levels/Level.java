package com.wordpress.rasanti.lightdream.levels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.gameobjects.Ground;
import com.wordpress.rasanti.lightdream.gameobjects.GroundPart;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;
import com.wordpress.rasanti.lightdream.gameobjects.events.Event;

public abstract class Level {

    private Vector2 size;

    public enum BLOCK_TYPE {
        EMPTY(255, 255, 255), // white
        GROUND(0, 255, 0), // green
        PLAYER_SPAWNPOINT(0, 0, 0), // black
        LIGHT_SPAWNPOINT(163,73,164); // pink

        private int color;

        private BLOCK_TYPE(int r, int g, int b) {
            color = r << 24 | g << 16 | b << 8 | 0xff;
        }

        public boolean sameColor(int color) {
            return this.color == color;
        }

        public int getColor() {
            return color;
        }
    }

    public List<GameObject> init() {
        List<GameObject> objects = new ArrayList<GameObject>();
        String filename = getFile();
        if (filename != null) {
            Map<Integer,Ground> verticalGrounds = new HashMap<Integer,Ground>();
            Ground groundHorizontal = null;
            // load image file that represents the level data
            Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));
            GroundPart groundPart = new GroundPart("groundSample", new Vector2(0,0));
            Sprite groundTex = groundPart.getAspect();
            // scan pixels from top-left to bottom-right
            size = new Vector2(pixmap.getWidth(),pixmap.getHeight());
            for (int pixelY = 0; pixelY < size.y ; pixelY++) {
                for (int pixelX = 0; pixelX < size.x; pixelX++) {
                    // get color of current pixel as 32-bit RGBA value
                    int currentPixel = pixmap.getPixel(pixelX, pixelY);
                    
                    if (!BLOCK_TYPE.GROUND.sameColor(currentPixel) && groundHorizontal != null ) {
                        objects.add(groundHorizontal);
                        groundHorizontal = null;
                    }
                    
                    if (pixelY > 0 && !BLOCK_TYPE.GROUND.sameColor(currentPixel) && BLOCK_TYPE.GROUND.sameColor(pixmap.getPixel(pixelX, pixelY-1)) ) {
                        if (verticalGrounds.get(pixelX) != null) {
                            objects.add(verticalGrounds.remove(pixelX));
                        }
                    }
                    
                    // find matching color value to identify block type at (x,y)
                    // point and create the corresponding game object if there is
                    // a match
                    // empty space
                    if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
                        // do nothing
                    } else if (BLOCK_TYPE.GROUND.sameColor(currentPixel)) {
                        if (pixelX > 0 && BLOCK_TYPE.GROUND.sameColor(pixmap.getPixel(pixelX-1, pixelY))) {
                            if (groundHorizontal == null) {
                                verticalGrounds.remove(pixelX-1);
                                groundHorizontal = new Ground("ground", new Vector2((pixelX-1) - (pixelX-1) * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f), true);
                                groundHorizontal.addPart(new GroundPart("ground", new Vector2((pixelX-1) - (pixelX-1) * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f)));
                            }
                            groundHorizontal.addPart(new GroundPart("ground", new Vector2(pixelX - pixelX * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f)));
                        } else if (pixelY > 0 && BLOCK_TYPE.GROUND.sameColor(pixmap.getPixel(pixelX, pixelY-1)) && verticalGrounds.get(pixelX) != null) {
                            verticalGrounds.get(pixelX).addPart(new GroundPart("ground", new Vector2(pixelX - pixelX * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f)));
                        } else {
                            verticalGrounds.put(pixelX, new Ground("ground", new Vector2(pixelX - pixelX * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f), false));
                            verticalGrounds.get(pixelX).addPart(new GroundPart("ground", new Vector2(pixelX - pixelX * groundTex.getWidth() / 1, 5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 8.9f)));
                        }
                    } else if (BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
                       Vector2 playerPosition = new Vector2(pixelX,5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 7);
                       objects.add(new Charlie("Charlie", playerPosition));
                   } else if (BLOCK_TYPE.LIGHT_SPAWNPOINT.sameColor(currentPixel)) {
                       Vector2 lightPosition = new Vector2(pixelX,5 + (pixmap.getHeight() - pixelY) - (pixmap.getHeight() - pixelY) * groundTex.getHeight() * 7);
                       objects.add(new Light("Light", lightPosition));
                   } else {
                       int r = 0xff & (currentPixel >>> 24); //red color channel
                       int g = 0xff & (currentPixel >>> 16); //green color channel
                       int b = 0xff & (currentPixel >>> 8); //blue color channel
                       int a = 0xff & currentPixel; //alpha channel
                       Gdx.app.error(getClass().getName(), "Unknown object at x<" + pixelX
                               + "> y<" + pixelY
                               + ">: r<" + r
                               + "> g<" + g
                               + "> b<" + b
                               + "> a<" + a + ">");
                   }
                }
            }
            if (groundHorizontal != null) {
                objects.add(groundHorizontal);
            }
            for (Map.Entry<Integer, Ground> e : verticalGrounds.entrySet()) {
                objects.add(e.getValue());
            }
            // free memory
            pixmap.dispose();
            Gdx.app.debug(getClass().getName(), "level '" + filename + "' loaded");
        }
        return objects;
    }
    
    protected PlayableCharacter findPlayer(List<GameObject> objects) {
        for (GameObject go : objects) {
            if (go instanceof Charlie) {
                return (PlayableCharacter) go;
            }
        }
        
        return null;
    }
    
    protected Light findLight(List<GameObject> objects) {
        for (GameObject go : objects) {
            if (go instanceof Light) {
                return (Light) go;
            }
        }
        
        return null;
    }
    
    public abstract String getFile();
    public abstract List<Event> getEvents();
    public abstract void onObjectsInitialized(List<GameObject> objects, WorldController world);
}
