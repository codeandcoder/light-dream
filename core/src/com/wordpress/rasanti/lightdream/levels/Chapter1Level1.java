package com.wordpress.rasanti.lightdream.levels;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.Background;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;
import com.wordpress.rasanti.lightdream.gameobjects.events.Event;
import com.wordpress.rasanti.lightdream.gameobjects.events.FallingEvent;
import com.wordpress.rasanti.lightdream.gui.GUI;

public class Chapter1Level1 extends Level {
    
    @Override
    public List<GameObject> init() {
        List<GameObject> objects = super.init();
        
        PlayableCharacter player = findPlayer(objects);
        Light light = findLight(objects);
        if (light != null) {
            light.setTarget(player);
        }
        
        Background background = new Background("background", new Vector2(-5, -10), "levels/black.png", 40, 30);
        objects.add(background);
        
        return objects;
    }
    
    @Override
    public String getFile() {
        return "levels/test_level.png";
    }
    
    @Override
    public List<Event> getEvents() {
        List<Event> events = new ArrayList<Event>();
        events.add(new FallingEvent("falling", new Vector2(-2,-50), new Vector2(100,50f), 1));
        return events;
    }

	@Override
	public void onObjectsInitialized(List<GameObject> objects, WorldController world) {
		PlayableCharacter player = world.player;
		if (player != null) {
            world.initialPosition = new Vector2(player.getPosition().x, player.getPosition().y);
            world.cameraHelper.setTarget(player);
            world.playerController.setCharacter(player);
        }
		GUI.getInstance().setScreen("play");
	}

}
