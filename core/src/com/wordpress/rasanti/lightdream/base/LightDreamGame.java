package com.wordpress.rasanti.lightdream.base;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordpress.rasanti.lightdream.controllers.CharacterController;
import com.wordpress.rasanti.lightdream.gui.GUI;

public class LightDreamGame extends ApplicationAdapter {
    
    private WorldController worldController;
    private boolean paused;
    private CharacterController controller;
    private SpriteBatch batch;
    private GUI gui;
    private boolean skippedFirstFrame = false;
    
    public LightDreamGame(CharacterController controller) {
    	this.controller = controller;
    }
    
    @Override public void create () { 
        // Set Libgdx log level to DEBUG
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        // Instantiate GUI manager and renderer
        gui = GUI.getInstance();
        batch = new SpriteBatch();
        
        paused = false;
    }
    @Override public void render () {
        
        // Sets the clear screen color to white
        Gdx.gl.glClearColor(255,255,255,0);
        // Clears the screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        batch.begin();
        // We skip the first frame loading in order to render the splash screen
        // while loading all data
        if (skippedFirstFrame) {
            if (worldController == null) {
                worldController = WorldController.getInstance();
                worldController.playerController = controller;
            }
            
            // Do not update the game world when paused
            if (!paused) {
                // Update game world by the time that has passed
                // since last rendered frame.
                worldController.update(Gdx.graphics.getDeltaTime());
            }
            // Render game world to screen
            worldController.render(batch);
        } else {
            skippedFirstFrame = true;
        }
        
        batch.end();
        
        if (worldController != null) {
            worldController.renderDebug();
        }
        
        gui.render(batch);
    }
    @Override public void resize (int width, int height) { 
        gui.resize(width, height);
        if (worldController != null) {
            worldController.resize(width, height);
        }
    }
    @Override public void pause () {
        paused = true;
    }
    @Override public void resume () {
        paused = false;
    }
    @Override public void dispose () {
        batch.dispose();
        worldController.dispose();
    }
}
