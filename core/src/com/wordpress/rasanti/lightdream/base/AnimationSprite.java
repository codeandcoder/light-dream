package com.wordpress.rasanti.lightdream.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationSprite {

    private String name;
    private Texture texture;
    private int rows;
    private int columns;
    private float speed;
    private TextureRegion[] frames;
    private boolean fliped;
    private Animation animation;
    private float size;
    private float aspectRelation;
    
    public AnimationSprite(String name, Texture texture, int rows, int columns, float speed, boolean fliped, float size) {
        this.name = name;
        this.texture = texture;
        this.rows = rows;
        this.columns = columns;
        this.speed = speed;
        this.fliped = fliped;
        this.size = size;
        this.aspectRelation = (texture.getWidth() * 1.0f / columns) / (texture.getHeight() * 1.0f / rows);
        animation = new Animation(speed, getFrames());
    }
    
    public String getName() {
        return name;
    }
    
    public float getSpeed() {
        return speed;
    }
    
    public float getAspectRelation() {
        return aspectRelation;
    }
    
    public TextureRegion[] getFrames() {
        if (frames == null) {
            TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/columns, texture.getHeight()/rows);
            frames = new TextureRegion[columns * rows];
            int index = 0;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    tmp[i][j].flip(fliped, false);
                    frames[index++] = tmp[i][j];
                }
            }
        }
        
        return frames;
    }
    
    public Animation getAnimation() {
        return animation;
    }
    
    public float getSize() {
        return size;
    }
    
    public int getRows() {
        return rows;
    }
    
    public int getColumns() {
        return columns;
    }
    
}
