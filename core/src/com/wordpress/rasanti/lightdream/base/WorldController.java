package com.wordpress.rasanti.lightdream.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.controllers.CharacterController;
import com.wordpress.rasanti.lightdream.gameobjects.AnimatedGameObject;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.gameobjects.Light;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;
import com.wordpress.rasanti.lightdream.gameobjects.events.Event;
import com.wordpress.rasanti.lightdream.levels.Level;
import com.wordpress.rasanti.lightdream.levels.MainLevel;
import com.wordpress.rasanti.lightdream.utils.DrawingComparator;

public class WorldController implements ContactListener {

    public static final boolean DEBUG_PHYISICS = false;
    public static String resFolder = "little/";
    private static WorldController world;
    
    public OrthographicCamera camera;
    public CameraHelper cameraHelper;
    public CharacterController playerController;
    public Vector2 initialPosition;
    private Level currentLevel;
    
    // GameObjects
    public List<GameObject> objects;
    public PlayableCharacter player;
    public Light light;
    
    // Physics
    public World physicsWorld;
    private Box2DDebugRenderer debugRenderer;
    private float accumulator = 0;

    private WorldController() {
        init();
    }
    
    public static WorldController getInstance() {
        if ( world == null ) {
            world = new WorldController();
        }
        
        return world;
    }

    private void init() {
        initBase();
        loadLevel(new MainLevel());
    }
    
    private void initBase() {
        // change textures size depending on the platform
    	if (Gdx.app.getType().equals(ApplicationType.Desktop)) {
    		//resFolder = "normal/";
    	}
        cameraHelper = new CameraHelper();
        objects = new ArrayList<GameObject>();
    }
    
    private void initRendering() {
        camera = new OrthographicCamera(Constants.VIEWPORT_WIDTH,
                Constants.VIEWPORT_HEIGHT);
        camera.position.set(0, 0, 0);
        camera.update();
    }
    
    private void initPhysics() {
        physicsWorld = new World(new Vector2(0, -10), true);
        debugRenderer = new Box2DDebugRenderer();
        for (GameObject go : objects) {
            go.initPhysics(physicsWorld, camera);
        }
        physicsWorld.setContactListener(this);
    }
    
    private void initEvents(Level level) {
        List<Event> events = level.getEvents();
        if (events != null) {
            objects.addAll(level.getEvents());
        }
    }
    
    public void loadLevel(Level level) {
    	if (player != null) {
    		player.destroy();
    	}
    	if (light != null) {
    		light.destroy();
    	}
    	for (int i = 0; i < objects.size(); i++) {
    		objects.get(i).destroy();
    	}
    	currentLevel = level;
        objects = level.init();
        getPlayer();
        getLight();
        initRendering();
        initPhysics();
        initEvents(level);
        currentLevel.onObjectsInitialized(objects, this);
        DrawingComparator comp = new DrawingComparator();
        Collections.sort(objects, comp);
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
    
    public PlayableCharacter getPlayer() {
        if (player == null) {
            for (GameObject obj : objects) {
                if (obj instanceof Charlie) {
                    player = (PlayableCharacter) obj;
                    break;
                }
            }
        }
        
        return player;
    }
    
    public Light getLight() {
        if (light == null) {
            for (GameObject obj : objects) {
                if (obj instanceof Light) {
                    light = (Light) obj;
                    break;
                }
            }
        }
        
        return light;
    }

    public void update(float deltaTime) {
        playerController.manageInput(deltaTime);
        handleDebugInput(deltaTime);
        
        // Update Physics
        // max frame time to avoid spiral of death (on slow devices)
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        while (accumulator >= Constants.TIME_STEP) {
            physicsWorld.step(Constants.TIME_STEP, Constants.VELOCITY_ITERATIONS, Constants.POSITION_ITERATIONS);
            accumulator -= Constants.TIME_STEP;
        }
        
        for (int i = 0; i < objects.size(); i++) {
            GameObject obj = objects.get(i);
            obj.update(deltaTime);
        }
        cameraHelper.update(deltaTime);
    }
    
    public void render(SpriteBatch batch) {
        cameraHelper.applyTo(camera);
        batch.setProjectionMatrix(camera.combined);
        
        for (GameObject obj : objects) {
        	if (!(obj instanceof PlayableCharacter)) {
        		obj.render(batch);
        	}
        }
        
        if (player != null) {
        	player.render(batch);
        }
        
        if (light != null) {
        	light.render(batch);
        }
    }
    
    public void renderDebug() {
        if (DEBUG_PHYISICS) {
            debugRenderer.render(physicsWorld, camera.combined);
        }
    }
    
    public void resize(int width, int height) {
        camera.viewportWidth = (Constants.VIEWPORT_HEIGHT / height) * width;
        camera.update();
    }
    
    public void dispose() {
        
    }

    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != ApplicationType.Desktop)
            return;

        // Camera Controls (move)
        float camMoveSpeed = 5 * deltaTime;
        float camMoveSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
            camMoveSpeed *= camMoveSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            moveCamera(-camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            moveCamera(camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.UP))
            moveCamera(0, camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.DOWN))
            moveCamera(0, -camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.BACKSPACE))
            cameraHelper.setPosition(0, 0);
        // Camera Controls (zoom)
        float camZoomSpeed = 1 * deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
            camZoomSpeed *= camZoomSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Keys.COMMA))
            cameraHelper.addZoom(camZoomSpeed);
        if (Gdx.input.isKeyPressed(Keys.PERIOD))
            cameraHelper.addZoom(-camZoomSpeed);
        if (Gdx.input.isKeyPressed(Keys.SLASH))
            cameraHelper.setZoom(1);
        
        if (Gdx.input.isKeyPressed(Keys.O)) {
            Filter filter = player.getPhysics().getFixtureList().get(0).getFilterData();
            filter.groupIndex = 1;
            player.getPhysics().getFixtureList().get(0).setFilterData(filter);
            light.getPhysics().getFixtureList().get(0).setFilterData(filter);
        }
            
    }

    private void moveCamera(float x, float y) {
        cameraHelper.setTarget(null);
        x += cameraHelper.getPosition().x;
        y += cameraHelper.getPosition().y;
        cameraHelper.setPosition(x, y);
    }

    @Override
    public void beginContact(Contact contact) {
        if (contact.getFixtureA().getBody().getUserData() instanceof AnimatedGameObject) {
            AnimatedGameObject gameObject = (AnimatedGameObject) contact.getFixtureA().getBody().getUserData();
            gameObject.collide(contact, (GameObject) contact.getFixtureB().getBody().getUserData());
        }
        if (contact.getFixtureB().getBody().getUserData() instanceof AnimatedGameObject) {
            AnimatedGameObject gameObject = (AnimatedGameObject) contact.getFixtureB().getBody().getUserData();
            gameObject.collide(contact, (GameObject) contact.getFixtureA().getBody().getUserData());
        }
    }
    
    @Override
    public void endContact(Contact contact) {
        if (contact.getFixtureA().getBody().getUserData() instanceof AnimatedGameObject) {
            AnimatedGameObject gameObject = (AnimatedGameObject) contact.getFixtureA().getBody().getUserData();
            gameObject.isolate(contact, (GameObject) contact.getFixtureB().getBody().getUserData());
        }
        if (contact.getFixtureB().getBody().getUserData() instanceof AnimatedGameObject) {
            AnimatedGameObject gameObject = (AnimatedGameObject) contact.getFixtureB().getBody().getUserData();
            gameObject.isolate(contact, (GameObject) contact.getFixtureA().getBody().getUserData());
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}

}
