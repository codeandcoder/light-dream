package com.wordpress.rasanti.lightdream.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.wordpress.rasanti.lightdream.base.WorldController;

public class SplashScreen extends Screen {

    public SplashScreen(String name) {
        super(name);
    }
    
    @Override
    public void init() {
        super.init();
        
        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();
        table.setFillParent(true);
        // table.debug();
        stage.addActor(table);
        
        Texture texture = new Texture(Gdx.files.internal(WorldController.resFolder + "other/splash.png"));
        
        float aspectRel = texture.getWidth() / texture.getHeight() * 1.0f;
        
        table.add(new Image(texture)).width(Gdx.graphics.getWidth()).height(Gdx.graphics.getWidth() / aspectRel).center();
    }
    
    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
    }

}
