package com.wordpress.rasanti.lightdream.gui;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class GUI {

    private static GUI gui;
    
    private Map<String,Screen> screens;
    private Screen currentScreen;
    
    private GUI() {
        screens = new HashMap<String, Screen>();
        
        Screen tmp = new MainScreen("main");
        screens.put(tmp.getName(), tmp);
        
        tmp = new PlayScreen("play");
        screens.put(tmp.getName(), tmp);
        
        tmp = new SplashScreen("splash");
        screens.put(tmp.getName(), tmp);
        
        setScreen("splash");
    }
    
    public static GUI getInstance() {
        if (gui == null) {
            gui = new GUI();
        }
        
        return gui;
    }
    
    public void setScreen(String screen) {
    	currentScreen = screens.get(screen);
    	currentScreen.init();
    }
    
    public void render(SpriteBatch batch) {
        currentScreen.render(batch);
    }
    
    public void resize(int width, int height) {
        currentScreen.resize(width, height);
    }
    
}
