package com.wordpress.rasanti.lightdream.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.utils.BlurUtils;

public class MainScreen extends Screen {
	
	private float title_ar;
	private float subtitle_ar;
	private float rest_ar;
    
    public MainScreen(String name) {
        super(name);
    }
    
    @Override
    public void init() {
        super.init();
        
        loadTitleTextures();
    }
    
    @Override
    public void resize(int width, int height) {
        initStage();
        loadTitleTextures();
        stage.getViewport().update(width, height, true);
    }
    
    private void loadTitleTextures() {
        //load original pixmap
        Pixmap title = new Pixmap(Gdx.files.internal(WorldController.resFolder + "other/dreamlight_title.png"));
        Pixmap subtitle = new Pixmap(Gdx.files.internal(WorldController.resFolder + "other/chapter1_title.png"));
        Pixmap rest = new Pixmap(Gdx.files.internal(WorldController.resFolder + "other/rest_title.png"));

        //Blur the original pixmap with a radius of 4 px
        //The blur is applied over 2 iterations for better quality
        //We specify "disposePixmap=true" to destroy the original pixmap
        Pixmap blurredTitle = BlurUtils.blur(title, 3, 4, true);
        Pixmap blurredSubtitle = BlurUtils.blur(subtitle, 4, 4, true);
        Pixmap blurredRest = BlurUtils.blur(rest, 4, 4, true);

        //we then create a GL texture with the blurred pixmap
        Texture blurTitle = new Texture(blurredTitle);
        Texture blurSubTitle = new Texture(blurredSubtitle);
        Texture blurRest = new Texture(blurredRest);
        
        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();
        table.setFillParent(true);
        // table.debug();
        stage.addActor(table);
        
        table.add(new Image(skin.newDrawable("white", Color.WHITE))).height(50).width(Gdx.graphics.getWidth());
        table.row();
        
        title_ar = blurTitle.getWidth() / blurTitle.getHeight() * 1.0f;
        subtitle_ar = blurSubTitle.getWidth() / blurSubTitle.getHeight() * 1.0f;
        rest_ar = blurRest.getWidth() / blurRest.getHeight() * 1.0f;
        
        int titleWidth = Math.round(Gdx.graphics.getWidth() * 0.7f);
        int subTitleWidth = Math.round(Gdx.graphics.getWidth() / 5f);
        int restWidth = Math.round(Gdx.graphics.getWidth() / 4);
        
        table.add(new Image(blurTitle)).size(titleWidth, titleWidth / title_ar).top().left().pad(10);
        table.row();
        table.add(new Image(blurSubTitle)).size(subTitleWidth, subTitleWidth / subtitle_ar).left().pad(10);
        table.row();
        table.add(new Image(skin.newDrawable("white", Color.WHITE))).height(Gdx.graphics.getHeight() - subTitleWidth / subtitle_ar - titleWidth / title_ar - restWidth / rest_ar - 100).width(1).left();
        table.row();
        table.add(new Image(blurRest)).size(restWidth, restWidth / rest_ar).right().bottom();
        
        stage.getRoot().setColor(1, 1, 1, 0);
        stage.getRoot().addAction(Actions.fadeIn(5));
        
        //dispose our blurred data now that it resides on the GPU
        blurredTitle.dispose();
        blurredSubtitle.dispose();
        blurredRest.dispose();
    }
    
}
