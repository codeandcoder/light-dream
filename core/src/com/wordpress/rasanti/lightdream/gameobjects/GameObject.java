package com.wordpress.rasanti.lightdream.gameobjects;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.base.WorldController;

public abstract class GameObject {
    
    protected String name;
    protected Vector2 position;
    protected float rotation;
    protected Sprite aspect;
    protected Body physics;
    protected WorldController world;
    protected boolean visible = true;
    protected int drawPriority = 0;
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aspect == null) ? 0 : aspect.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((physics == null) ? 0 : physics.hashCode());
        result = prime * result
                + ((position == null) ? 0 : position.hashCode());
        result = prime * result + Float.floatToIntBits(rotation);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameObject other = (GameObject) obj;
        if (aspect == null) {
            if (other.aspect != null)
                return false;
        } else if (!aspect.equals(other.aspect))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (physics == null) {
            if (other.physics != null)
                return false;
        } else if (!physics.equals(other.physics))
            return false;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        if (Float.floatToIntBits(rotation) != Float
                .floatToIntBits(other.rotation))
            return false;
        return true;
    }

    public GameObject(String name, Vector2 position) {
        this.name = name;
        this.position = position;
    }

    public Vector2 getPosition() {
        return position;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Body getPhysics() {
        return physics;
    }
    
    public float getRotation() {
        return rotation;
    }

    public Sprite getAspect() {
        return aspect;
    }

    public void setAspect(Sprite aspect) {
        this.aspect = aspect;
    }

    public WorldController getWorld() {
        return world;
    }

    public void setWorld(WorldController world) {
        this.world = world;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setPhysics(Body physics) {
        this.physics = physics;
    }
    
    public int getDrawPriority() {
        return drawPriority;
    }
    
    public void destroy() {
    	if (aspect != null) {
    		aspect.getTexture().dispose();
    	}
    	if (physics != null) {
    		for (int i = 0; i < physics.getFixtureList().size; i++) {
            	physics.destroyFixture(physics.getFixtureList().get(i));
        	}
    	}
        WorldController world = WorldController.getInstance();
        if (this instanceof Charlie) {
            world.player = null;
        } else if (this instanceof Light) {
        	world.light = null;
        }
        world.objects.remove(this);
    }

    public abstract void initPhysics(World world, OrthographicCamera camera);
    public abstract void update(float deltaTime);
    public abstract void render(SpriteBatch batch);

}
