package com.wordpress.rasanti.lightdream.gameobjects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.base.AnimationSprite;
import com.wordpress.rasanti.lightdream.base.WorldController;

public class Charlie extends PlayableCharacter {

    public static final String IDLE_L = "idle.L", IDLE_R = "idle.R", RUN_L = "run.L", RUN_R = "run.R"; 
    private static final float SIZE = 0.2f;
    private float XMoving;
    private Fixture fixture;
    private boolean isJumping = false;
    
    public Charlie(String name, Vector2 position) {
        super(name, position);
    }
    
    @Override
    protected List<AnimationSprite> getAnimationSrites() {
        List<AnimationSprite> anims = new ArrayList<AnimationSprite>();
//        anims.add(new AnimationSprite(IDLE_L, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/idle.png")), 1, 15, 0.1f, true, SIZE));
//        anims.add(new AnimationSprite(IDLE_R, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/idle.png")), 1, 15, 0.1f, false, SIZE));
//        anims.add(new AnimationSprite(RUN_L, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/run_lite.png")), 1, 20, 0.05f, true, SIZE));
//        anims.add(new AnimationSprite(RUN_R, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/run_lite.png")), 1, 20, 0.05f, false, SIZE));
        anims.add(new AnimationSprite(IDLE_L, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/charlie_idle.png")), 1, 15, 0.1f, true, SIZE));
        anims.add(new AnimationSprite(IDLE_R, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/charlie_idle.png")), 1, 15, 0.1f, false, SIZE));
        anims.add(new AnimationSprite(RUN_L, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/charlie_run.png")), 1, 20, 0.05f, true, SIZE  * 1.85f));
        anims.add(new AnimationSprite(RUN_R, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/charlie_run.png")), 1, 20, 0.05f, false, SIZE * 1.85f));
        return anims;
    }

    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
     // First we create a body definition
        BodyDef bodyDef = new BodyDef();
        // We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
        bodyDef.type = BodyType.DynamicBody;
        // Set our body's starting position in the world
        bodyDef.position.set(position.x, position.y);

        // Create our body in the world using our body definition
        physics = world.createBody(bodyDef);
        physics.setFixedRotation(true);

        // Create a polygon shape
        PolygonShape box = new PolygonShape();  
        // Set the polygon shape as a box which is twice the size of our view port and 20 high
        // (setAsBox takes half-width and half-height as arguments)
        box.setAsBox(SIZE / 3, SIZE);

        // Create a fixture definition to apply our shape to
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = box;
        fixtureDef.density = 0.75f; 
        fixtureDef.friction = 0.9f;
        fixtureDef.restitution = 0.0f;
        fixtureDef.filter.groupIndex = -1;

        // Create our fixture and attach it to the body
        fixture = physics.createFixture(fixtureDef);
        physics.setUserData(this);
        
        // Remember to dispose of any shapes after you're done with them!
        // BodyDef and FixtureDef don't need disposing, but shapes do.
        box.dispose();
    }
    
    public void move(float x, float y, float deltaTime) {
        XMoving = x;
        physics.setLinearVelocity(x, y);
    }
    
    public void jump() {
        if (isGrounded() && !isJumping) {
            physics.applyLinearImpulse(0, 0.11f, position.x, position.y, true);
            isJumping = true;
        }
    }
    
    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        position.x = physics.getPosition().x - currentAnimation.getSize() / 2f;
        position.y = physics.getPosition().y - 0.25f;
        
        // Update animation
        if (XMoving > 1) {
            currentAnimation = animations.get(RUN_R);
            XMoving = 1;
        } else if (XMoving < -1) {
            currentAnimation = animations.get(RUN_L);
            XMoving = -1;
        } else {
            currentAnimation = XMoving == 1 ? animations.get(IDLE_R) : animations.get(IDLE_L);
        }
    }

    @Override
    public void collide(Contact contact, GameObject object) {
        super.collide(contact, object);
        if (isGrounded()) {
            contact.setFriction(0.9f);
            fixture.setFriction(0.9f);
            isJumping = false;
        }
    }

    @Override
    public void isolate(Contact contact, GameObject object) {
        super.isolate(contact, object);
        if (!isGrounded()) {
            contact.setFriction(0f);
            fixture.setFriction(0f);
        }
    }

}
