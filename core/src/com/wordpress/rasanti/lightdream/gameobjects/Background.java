package com.wordpress.rasanti.lightdream.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class Background extends GameObject{

    private Texture texture;
    private float xSize;
    private float ySize;
    
    public Background(String name, Vector2 position, String texture, float xSize, float ySize) {
        super(name, position);
        this.texture = new Texture(Gdx.files.internal(texture));
        this.xSize = xSize;
        this.ySize = ySize;
        this.drawPriority = -1;
    }
    
    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
        // do nothing
    }

    @Override
    public void update(float deltaTime) {
        // do nothing
    }

    @Override
    public void render(SpriteBatch batch) {
        if (visible) {
            aspect = new Sprite(texture);
            aspect.setSize(xSize, ySize);
            aspect.setOrigin(aspect.getWidth() / 2, aspect.getHeight() / 2);
            aspect.setPosition(position.x, position.y);
            aspect.setRotation(rotation);
            aspect.draw(batch);
        }
    }

}
