package com.wordpress.rasanti.lightdream.gameobjects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.base.AnimationSprite;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.utils.Utils;

public class Light extends PlayableCharacter {

    public static final String IDLE = "idle";
    public static final float SIZE = 0.25f;
    public static final float FOLLOWING_DISTANCE = 0.1f;
    public static final float FOLLOWING_SPEED = 2;
    public static final float LINK_DISTANCE = 2;
    public float animCounter;
    private GameObject target;
    private boolean moveY;
    
    public Light(String name, Vector2 position) {
        super(name, position);
    }

    @Override
    protected List<AnimationSprite> getAnimationSrites() {
        List<AnimationSprite> anims = new ArrayList<AnimationSprite>();
        anims.add(new AnimationSprite(IDLE, new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/light.png")), 1, 4, 0.1f, false, SIZE));
        return anims;
    }

    @Override
    public void move(float x, float y, float deltaTime) {
        moveY = y != physics.getLinearVelocity().y;
        physics.setLinearVelocity(x, y);
    }

    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
            // First we create a body definition
           BodyDef bodyDef = new BodyDef();
           // We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
           bodyDef.type = BodyType.KinematicBody;
           // Set our body's starting position in the world
           bodyDef.position.set(position.x, position.y);

           // Create our body in the world using our body definition
           physics = world.createBody(bodyDef);
           physics.setFixedRotation(true);

           // Create a circle shape and set its radius to 6
           CircleShape circle = new CircleShape();
           circle.setRadius(SIZE / 5);
           
           // Create a fixture definition to apply our shape to
           FixtureDef fixtureDef = new FixtureDef();
           fixtureDef.shape = circle;
           fixtureDef.density = 0.5f; 
           fixtureDef.friction = 1f;
           fixtureDef.restitution = 0.0f;
           fixtureDef.filter.groupIndex = -1;
           
           // Create our fixture and attach it to the body
           physics.createFixture(fixtureDef);
           physics.setUserData(this);
           
           // Remember to dispose of any shapes after you're done with them!
           // BodyDef and FixtureDef don't need disposing, but shapes do.
           circle.dispose();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        position.x = physics.getPosition().x - SIZE / 2;
        position.y = physics.getPosition().y - SIZE / 2;
        
        // Update animation
        animCounter += deltaTime;
        float xVel = physics.getLinearVelocity().x;
        float yVel = moveY ? physics.getLinearVelocity().y : 0;
        yVel += (float) Math.sin(animCounter) / 5f;
       
        if (target != null && target.getAspect() != null && Utils.distance(target.getPosition(), position) > FOLLOWING_DISTANCE) {
            xVel = (target.getPosition().x - position.x) + 0.05f * FOLLOWING_SPEED;
            yVel += (target.getPosition().y - position.y) + 0.2f * FOLLOWING_SPEED;
        } else if (target == null && WorldController.getInstance().player != null && Utils.distance(WorldController.getInstance().player.getPosition(), position) > LINK_DISTANCE) {
            if (WorldController.getInstance().playerController.getCharacter().equals(this)) {
                WorldController.getInstance().playerController.switchMode();
            } else {
                setCollision(false);
                setTarget(WorldController.getInstance().player);
            }
        }
        
        physics.setLinearVelocity(xVel, yVel);
        if (this.equals(WorldController.getInstance().playerController.getCharacter())) {
        	brake();
        }
        moveY = false;
    }
    
    private void brake() {
        float brakeRate = 0.08f;
        float currentYVel = physics.getLinearVelocity().y;
        float currentXVel = physics.getLinearVelocity().x;
        
        float newXVel = currentXVel > 0 ? currentXVel - brakeRate : currentXVel < 0 ? currentXVel + brakeRate : 0;
        float newYVel = currentYVel > 0 ? currentYVel - brakeRate : currentYVel < 0 ? currentYVel + brakeRate : 0;
        
        if (newXVel > -brakeRate && newXVel < brakeRate) {
            newXVel = 0;
        }
        if (newYVel > -brakeRate && newYVel < brakeRate) {
            newYVel = 0;
        }
        
        physics.setLinearVelocity(newXVel, newYVel);
    }

    @Override
    public void collide(Contact contact, GameObject object) {
        super.collide(contact, object);
    }

    @Override
    public void isolate(Contact contact, GameObject object) {
        super.isolate(contact, object);
    }
    
    public GameObject getTarget() {
        return target;
    }
    
    public void setTarget(GameObject target) {
        this.target = target;
    }
    
    public void setCollision(boolean active) {
        if (active) {
            Filter filter = physics.getFixtureList().get(0).getFilterData();
            filter.groupIndex = 1;
            physics.getFixtureList().get(0).setFilterData(filter);
        } else {
            Filter filter = physics.getFixtureList().get(0).getFilterData();
            filter.groupIndex = -1;
            physics.getFixtureList().get(0).setFilterData(filter);
        }
    }
}
