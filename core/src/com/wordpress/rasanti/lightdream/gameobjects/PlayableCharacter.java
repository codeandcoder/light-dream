package com.wordpress.rasanti.lightdream.gameobjects;

import com.badlogic.gdx.math.Vector2;

public abstract class PlayableCharacter extends AnimatedGameObject {

    public PlayableCharacter(String name, Vector2 position) {
        super(name, position);
    }
}
