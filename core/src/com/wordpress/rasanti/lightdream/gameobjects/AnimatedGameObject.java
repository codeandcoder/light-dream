package com.wordpress.rasanti.lightdream.gameobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.wordpress.rasanti.lightdream.base.AnimationSprite;

public abstract class AnimatedGameObject extends GameObject {

    protected AnimationSprite currentAnimation;
    protected Map<String, AnimationSprite> animations;
    private float stateTime;
    protected List<GameObject> grounds;
    protected Vector2 velocity;

    public AnimatedGameObject(String name, Vector2 position) {
        super(name, position);
        init();
    }

    private void init() {
    	grounds = new ArrayList<GameObject>();
        animations = new HashMap<String, AnimationSprite>();
        List<AnimationSprite> anims = getAnimationSrites();
        for (AnimationSprite a : anims) {
            animations.put(a.getName(), a);
            currentAnimation = a;
        }
    }
    
    @Override
    public void render(SpriteBatch batch) {
    	if (visible) {
    		stateTime += Gdx.graphics.getDeltaTime();
    		TextureRegion currentTexture = currentAnimation.getAnimation().getKeyFrame(stateTime, true);
    		float relAspect = currentAnimation.getAspectRelation();
        	aspect = new Sprite(currentTexture);
        	aspect.setSize(currentAnimation.getSize(), currentAnimation.getSize() / relAspect);
        	aspect.setOrigin(aspect.getWidth() / 2, aspect.getHeight() / 2);
        	aspect.setPosition(position.x, position.y);
        	aspect.setRotation(rotation);
        	aspect.draw(batch);
    	}
    }
    
    @Override
    public void update(float deltaTime) {
        
    }
    
    public AnimationSprite getCurrentAnimation() {
        return currentAnimation;
    }
    
    public boolean isGrounded() {
        return !grounds.isEmpty();
    }

    protected abstract List<AnimationSprite> getAnimationSrites();

    public abstract void move(float x, float y, float deltaTime);
    
    public void collide(Contact contact, GameObject object) {
        float yValue = position.y - object.getPosition().y;
        if (object instanceof Ground) {
            Ground ground = (Ground) object;
            if (ground.isHorizontal()) {
                yValue += object.getAspect().getHeight() / 10;
            } else {
                yValue += (object.getAspect().getHeight() / 10) * ground.getParts().size();
            }
        }
        if (yValue > 0) {
            grounds.add(object);
        }
    }
    public void isolate(Contact contact, GameObject object) {
        grounds.remove(object);
    }
}
