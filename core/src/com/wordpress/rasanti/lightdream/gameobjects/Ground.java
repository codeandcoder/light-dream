package com.wordpress.rasanti.lightdream.gameobjects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;


public class Ground extends GameObject {

    private boolean isHorizontal;
    private List<GroundPart> parts;
    
    public Ground(String name, Vector2 position, boolean horizontal) {
        super(name, position);
        parts = new ArrayList<GroundPart>();
        isHorizontal = horizontal;
    }
    
    public void addPart(GroundPart part) {
    	if (aspect == null) {
    		aspect = part.getAspect();
    	}
        parts.add(part);
    }

    @Override
    public void render(SpriteBatch batch) {
        for (GroundPart part : parts) {
            part.render(batch);
        }
    }
    
    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
        // Create our body definition
        BodyDef groundBodyDef = new BodyDef();  
        // Set its world position
        float halfWidth = parts.get(0).getAspect().getWidth() / 2;
        float halfHeight = parts.get(0).getAspect().getHeight() / 2;
        if (isHorizontal) {
            groundBodyDef.position.set(new Vector2(position.x + halfWidth * (parts.size()), position.y + halfHeight));  
        } else {
            groundBodyDef.position.set(new Vector2(position.x + halfWidth, position.y - halfHeight * (parts.size() - 1.5f)));  
        }
        // Create a body from the defintion and add it to the world
        physics = world.createBody(groundBodyDef);  

        // Create a polygon shape
        PolygonShape groundBox = new PolygonShape();  
        // Set the polygon shape as a box which is twice the size of our view port and 20 high
        // (setAsBox takes half-width and half-height as arguments)
        if (isHorizontal) {
            groundBox.setAsBox(halfWidth * parts.size(), halfHeight);
        } else {
            groundBox.setAsBox(halfWidth, halfHeight * (parts.size() + 0.5f));
        }
        // Create a fixture from our polygon shape and add it to our ground body  
        physics.createFixture(groundBox, 0.0f); 
        physics.setUserData(this);
        // Clean up after ourselves
        groundBox.dispose();
    }

    @Override
    public void update(float deltaTime) {
        // do nothing
    }
    
    public boolean isHorizontal() {
        return isHorizontal;
    }
    
    public List<GroundPart> getParts() {
        return parts;
    }

}
