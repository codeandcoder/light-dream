package com.wordpress.rasanti.lightdream.gameobjects.events;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.levels.Chapter1Level1;

public class SomeInputEvent extends Event {

	public SomeInputEvent(String name, Vector2 position, Vector2 size,
			float checkFrequency) {
		super(name, position, size, checkFrequency);
	}

	@Override
	public void trigger(GameObject o) {
		WorldController world = WorldController.getInstance();
		if (world.playerController.someInputReceived()) {
			world.loadLevel(new Chapter1Level1());
		}
	}

}
