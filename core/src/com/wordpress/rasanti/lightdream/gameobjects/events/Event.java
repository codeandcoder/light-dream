package com.wordpress.rasanti.lightdream.gameobjects.events;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;

public abstract class Event extends GameObject {

    private float lastCheck;
    private float time;
    private boolean enabled;
    private Vector2 size;
    private float checkFrequency;
    
    public Event(String name, Vector2 position, Vector2 size, float checkFrequency) {
        super(name, position);
        this.size = size;
        this.enabled = true;
        this.time = 0;
        this.lastCheck = 0;
        this.checkFrequency = checkFrequency;
    }

    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
        // do nothing
    }

    @Override
    public void update(float deltaTime) {
        time += deltaTime;
        if (time - lastCheck >= checkFrequency && enabled) {
            for (int i = 0; i < WorldController.getInstance().objects.size(); i++) {
                GameObject go = WorldController.getInstance().objects.get(i);
                if (isInRange(go.getPosition())) {
                    trigger(go);
                }
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        // do nothing
    }
    
    private boolean isInRange(Vector2 target) {
        boolean inXRange = target.x > position.x && target.x < position.x + size.x;
        boolean inYRange = target.y > position.y && target.y < position.y + size.y;
        return inXRange && inYRange;
    }
    
    public abstract void trigger(GameObject o);
    
}
