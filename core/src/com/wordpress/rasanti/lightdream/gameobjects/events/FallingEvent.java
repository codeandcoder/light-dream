package com.wordpress.rasanti.lightdream.gameobjects.events;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.lightdream.base.WorldController;
import com.wordpress.rasanti.lightdream.gameobjects.Charlie;
import com.wordpress.rasanti.lightdream.gameobjects.GameObject;
import com.wordpress.rasanti.lightdream.gameobjects.PlayableCharacter;

public class FallingEvent extends Event {

    public FallingEvent(String name, Vector2 position, Vector2 size, float checkFrequency) {
        super(name, position, size, checkFrequency);
    }

    @Override
    public void trigger(GameObject o) {
        if ( o instanceof PlayableCharacter ) {
            WorldController world = WorldController.getInstance();
            boolean setLightTarget = world.light.getTarget() != null;
            world.player.destroy();
            world.player = new Charlie("charlie", new Vector2(world.initialPosition.x, world.initialPosition.y));
            world.player.initPhysics(world.physicsWorld, world.camera);
            world.cameraHelper.setTarget(world.player);
            world.playerController.setCharacter(world.player);
            if (setLightTarget) {
                world.light.setTarget(world.player);
            }
            world.objects.add(world.player);

            Gdx.app.debug(getClass().getName(), "player respawned");
        }
    }

}
