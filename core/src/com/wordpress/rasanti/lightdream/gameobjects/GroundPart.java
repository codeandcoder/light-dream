package com.wordpress.rasanti.lightdream.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.wordpress.rasanti.lightdream.base.WorldController;

public class GroundPart extends GameObject {

	private static final float SIZEX = 0.5f;
	private static final float SIZEY = 0.1f;
	private TextureRegion currentTexture;
    
    public GroundPart(String name, Vector2 position) {
        super(name, position);
        currentTexture = new TextureRegion(new Texture(Gdx.files.internal(WorldController.resFolder + "sprites/cube.png")));
        aspect = new Sprite(currentTexture);
        aspect.setSize(SIZEX, SIZEY);
        aspect.setOrigin(aspect.getWidth() / 2, aspect.getHeight() / 2);
        aspect.setPosition(position.x, position.y);
    }

    @Override
    public void render(SpriteBatch batch) {
        aspect = new Sprite(currentTexture);
        aspect.setSize(SIZEX, SIZEY);
        aspect.setOrigin(aspect.getWidth() / 2, aspect.getHeight() / 2);
        aspect.setPosition(position.x, position.y);
        aspect.draw(batch);
    }
    
    @Override
    public void initPhysics(World world, OrthographicCamera camera) {
        // do nothing
    }

    @Override
    public void update(float deltaTime) {
        // do nothing
    }
    
}
