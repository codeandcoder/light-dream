package com.wordpress.rasanti.lightdream.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.wordpress.rasanti.lightdream.base.LightDreamGame;
import com.wordpress.rasanti.lightdream.controllers.DesktopController;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new LightDreamGame(new DesktopController());
        }
}